import 'package:get/get.dart';
import 'package:to_do_list_app/bindings/splash_bindings.dart';
import 'package:to_do_list_app/ui/register_page.dart';
import 'package:to_do_list_app/ui/splash_page.dart';
import 'package:to_do_list_app/ui/to_do_list_page.dart';

import '../bindings/authentication_bindings.dart';
import '../bindings/to_do_list_bindings.dart';
import '../controller/authentication_controller.dart';
import '../ui/login_page.dart';

class Routes {
  static List<GetPage<dynamic>>? listPage() {
    return [
      GetPage(
          name: SplashPage.routeName,
          page: () => SplashPage(),
          binding: SplashBindings()),
      GetPage(
          name: ToDoListPage.routeName,
          page: () => ToDoListPage(),
          binding: ToDoListBindings()),
      GetPage(
          name: LoginPage.routeName,
          page: () => LoginPage(),
          binding: AuthenticationBindings()),
          GetPage(
          name: RegisterPage.routeName,
          page: () => RegisterPage(),
          binding: AuthenticationBindings())
    ];
  }
}
