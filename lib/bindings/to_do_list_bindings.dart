import 'package:get/get.dart';
import 'package:to_do_list_app/controller/to_do_list_controller.dart';

class ToDoListBindings implements Bindings {
  @override
  void dependencies() {
    Get.put<ToDoListController>(ToDoListController());
  }
}