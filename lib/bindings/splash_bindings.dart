import 'package:get/get.dart';
import 'package:to_do_list_app/controller/splash_page_controller.dart';

class SplashBindings implements Bindings {
  @override
  void dependencies() {
    Get.put<SplashPageController>(SplashPageController());
  }
}