import 'package:get/get.dart';
import 'package:to_do_list_app/controller/authentication_controller.dart';

class AuthenticationBindings implements Bindings {
  @override
  void dependencies() {
    Get.put<AuthenticationController>(AuthenticationController());
  }
}