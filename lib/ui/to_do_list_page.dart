import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:to_do_list_app/controller/to_do_list_controller.dart';
import 'package:to_do_list_app/model/task_model.dart';
import 'package:sizer/sizer.dart';
import 'package:to_do_list_app/reusable_widget/customize_button.dart';
import 'package:to_do_list_app/reusable_widget/primary_button.dart';
import 'package:to_do_list_app/reusable_widget/primary_textfield.dart';
import 'package:uuid/uuid.dart';

class ToDoListPage extends StatelessWidget {
  static const routeName = '/to-do-list-page';
  ToDoListPage({super.key});

  final ToDoListController _toDoListController = ToDoListController.find;

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: _toDoListController,
        builder: (controller) {
          debugPrint('controller: ${controller.listTask}');
          return WillPopScope(
            onWillPop: () async {
              _showOutAppBottomSheet(context);
              return false;
            },
            child: Scaffold(
              appBar: AppBar(
                title: Text(controller.title.string),
                actions: [
                  GestureDetector(
                    onTap: () {
                      _showLogoutAppBottomSheet(context);
                    },
                    child: const Icon(
                      Icons.exit_to_app,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
              body: Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.grey[300],
                child: !controller.isEmpty
                    ? SingleChildScrollView(
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            _buildSearchTask(),
                            const SizedBox(
                              height: 10,
                            ),
                            _buildDropdownSort(),
                            const SizedBox(
                              height: 10,
                            ),
                            _buildListOfTodo(controller.listTask),
                          ],
                        ),
                      )
                    : const Center(
                        child: Text(
                            'There is no task, please click the plus icon :D'),
                      ),
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  _toDoListController.taskTextEditingController.clear();
                  _showAddTaskBottomSheet(context);
                },
                child: const Icon(Icons.add),
              ),
            ),
          );
        });
  }

  Widget _buildListOfTodo(List<TaskModel> listTask) {
    return ListView.builder(
        itemCount: listTask.length,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              debugPrint(
                  'TaskModelToJson: ${taskModelToJson(listTask[index])}');
              debugPrint(
                  'List TaskModelToJson: ${listOfTaskModelToJson(listTask)}');
              debugPrint('largest id: ${_toDoListController.id}');
            },
            child: Container(
              width: double.infinity,
              color: Colors.white,
              margin: const EdgeInsets.only(top: 10),
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      color: Colors.transparent,
                      child: Text(listTask[index].task),
                    ),
                  ),
                  listTask[index].status == 'done'
                      ? Container(
                          color: Colors.transparent,
                          child: Row(
                            // ignore: prefer_const_literals_to_create_immutables
                            children: [
                              const Icon(
                                Icons.done,
                                color: Colors.blue,
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              const Text('Finish'),
                              const SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                onTap: () {
                                  _toDoListController
                                      .selectedTask(listTask[index]);
                                  _toDoListController.selectedIndex(index);
                                  _showDeleteTaskBottomSheet(context);
                                },
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  color: Colors.grey,
                                  child: const Icon(
                                    Icons.close,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      : Container(
                          color: Colors.transparent,
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  _toDoListController.editTaskStatus(
                                      listTask[index].uuid, TaskStatus.DONE);
                                },
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  color: Colors.lightBlue,
                                  child: const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                onTap: () {
                                  _toDoListController.taskTextEditingController
                                      .clear();
                                  _toDoListController.selectedIndex(index);
                                  _toDoListController
                                      .selectedTask(listTask[index]);
                                  _showEditTaskBottomSheet(context);
                                },
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  color: Colors.green,
                                  child: const Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                onTap: () {
                                  _toDoListController
                                      .selectedTask(listTask[index]);
                                  _toDoListController.selectedIndex(index);
                                  _showDeleteTaskBottomSheet(context);
                                },
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  color: Colors.red,
                                  child: const Icon(
                                    Icons.delete,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                ],
              ),
            ),
          );
        });
  }

  Widget _buildSearchTask() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        width: double.infinity,
        height: 45,
        color: Colors.white,
        child: Row(
          children: [
            Container(
              width: 45,
              height: 45,
              color: Colors.grey,
              child: const Icon(Icons.search),
            ),
            Expanded(
              child: PrimaryTextField(
                controller:
                    _toDoListController.keywordSearchTextEditingController,
                hintText: 'Enter Your Task',
                onChanged: (v) {
                  _toDoListController.searchTask(v);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDropdownSort() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        color: Colors.white,
        width: double.infinity,
        child: Row(
          children: [
            Container(
              width: 45,
              height: 45,
              color: Colors.grey,
              child: const Icon(Icons.sort),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Container(
                  color: Colors.white,
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      isExpanded: true,
                      // Initial Value

                      value: _toDoListController.initValue,

                      // Down Arrow Icon
                      icon: const Icon(Icons.keyboard_arrow_down),

                      // Array list of items
                      items: _toDoListController.listSort.map((String items) {
                        return DropdownMenuItem(
                          value: items,
                          child: Text(items),
                        );
                      }).toList(),
                      // After selecting the desired option,it will
                      // change button value to selected value
                      onChanged: (String? value) {
                        _toDoListController.updateInitValue(value!);
                        switch (value) {
                          case 'Newest':
                            _toDoListController.sortTask(SortBy.NEWEST);
                            break;
                          case 'Oldest':
                            _toDoListController.sortTask(SortBy.OLDEST);
                            break;
                          case 'Taskname':
                            _toDoListController.sortTask(SortBy.TASK_NAME);
                            break;
                          case 'Status':
                            _toDoListController.sortTask(SortBy.STATUS);
                            break;
                          default:
                        }
                      },
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _showAddTaskBottomSheet(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      margin: const EdgeInsets.only(top: 10),
                      child: Container(
                        color: Colors.transparent,
                        child: PrimaryTextField(
                          controller:
                              _toDoListController.taskTextEditingController,
                          hintText: 'Enter Your Task',
                        ),
                      ))),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      width: double.infinity,
                      height: 45,
                      margin: const EdgeInsets.only(bottom: 10),
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          child: PrimaryButton(
                            onPressed: () {
                              if (_toDoListController
                                  .taskTextEditingController.text.isNotEmpty) {
                                DateTime now = DateTime.now();
                                var uuid = Uuid();
                                _toDoListController.addTask(TaskModel(
                                    id: _toDoListController.nextId(),
                                    uuid: uuid.v4(),
                                    task: _toDoListController
                                        .taskTextEditingController.text,
                                    status: 'to_do',
                                    date: DateTime(
                                        now.year,
                                        now.month,
                                        now.day,
                                        now.hour,
                                        now.minute,
                                        now.second,
                                        now.millisecond,
                                        now.microsecond)));
                                Get.back();
                              } else {
                                Fluttertoast.showToast(
                                    msg: 'Form cannot be empty',
                                    backgroundColor: Colors.red);
                              }
                            },
                            text: 'Add',
                          ))))
            ],
          ),
        ));
  }

  _showEditTaskBottomSheet(BuildContext context) {
    _toDoListController.taskTextEditingController.text =
        _toDoListController.selectTask.task;
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      margin: const EdgeInsets.only(top: 10),
                      child: Container(
                        color: Colors.transparent,
                        child: PrimaryTextField(
                          controller:
                              _toDoListController.taskTextEditingController,
                          hintText: 'Enter Your Task',
                        ),
                      ))),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      width: double.infinity,
                      height: 45,
                      margin: const EdgeInsets.only(bottom: 10),
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          child: PrimaryButton(
                            onPressed: () {
                              if (_toDoListController
                                  .taskTextEditingController.text.isNotEmpty) {
                                _toDoListController.editTask(
                                    _toDoListController.selectTask.uuid,
                                    _toDoListController
                                        .taskTextEditingController.text,
                                    _toDoListController.indexList!);
                                Get.back();
                              } else {
                                Fluttertoast.showToast(
                                    msg: 'Form cannot be empty',
                                    backgroundColor: Colors.red);
                              }
                            },
                            text: 'Save',
                          ))))
            ],
          ),
        ));
  }

  _showDeleteTaskBottomSheet(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          margin: const EdgeInsets.only(top: 10),
                          child: const Text(
                              'Are You sure to delete this task ?')))),
              Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: CustomizeButton(
                                  backgroundColor: Colors.red,
                                  onPressed: () {
                                    Get.back();
                                    _toDoListController.deleteTask(
                                        _toDoListController.selectTask.id,
                                        _toDoListController.indexList!);
                                  },
                                  text: 'Yes',
                                ))),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: PrimaryButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  text: 'No',
                                ))),
                      ),
                    ],
                  ))
            ],
          ),
        ));
  }

  _showLogoutAppBottomSheet(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          margin: const EdgeInsets.only(top: 10),
                          child: const Text(
                              'Are You sure to logout this app ?')))),
              Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: CustomizeButton(
                                  backgroundColor: Colors.red,
                                  onPressed: () {
                                    Get.back();
                                    _toDoListController.logoutApp();
                                  },
                                  text: 'Yes',
                                ))),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: PrimaryButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  text: 'No',
                                ))),
                      ),
                    ],
                  ))
            ],
          ),
        ));
  }

  _showOutAppBottomSheet(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          margin: const EdgeInsets.only(top: 10),
                          child:
                              const Text('Are You sure to close this app ?')))),
              Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: CustomizeButton(
                                  backgroundColor: Colors.red,
                                  onPressed: () {
                                    exit(0);
                                  },
                                  text: 'Yes',
                                ))),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: PrimaryButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  text: 'No',
                                ))),
                      ),
                    ],
                  ))
            ],
          ),
        ));
  }
}
