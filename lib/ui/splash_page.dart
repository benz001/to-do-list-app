import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:to_do_list_app/controller/splash_page_controller.dart';

class SplashPage extends StatelessWidget {
  static const routeName = '/splash-page';
  SplashPage({super.key});
  final SplashPageController _splashPageController = SplashPageController.find;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.white,
      alignment: Alignment.center,
      child: GetBuilder(
          init: _splashPageController,
          builder: (controller) {
            return Text(
              controller.welcomeText,
              style: const TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
            );
          }),
    ));
  }
}
