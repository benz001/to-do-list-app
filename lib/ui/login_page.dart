import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:to_do_list_app/controller/authentication_controller.dart';
import 'package:to_do_list_app/reusable_widget/primary_button.dart';
import 'package:to_do_list_app/reusable_widget/primary_textfield.dart';
import 'package:to_do_list_app/ui/register_page.dart';

import '../reusable_widget/primary_button_loading.dart';

class LoginPage extends StatelessWidget {
  LoginPage({super.key});
  static const routeName = '/login-page';

  final AuthenticationController _authhenticationController =
      AuthenticationController.find;

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: _authhenticationController,
      builder: (controller) {
        return Scaffold(
          body: SafeArea(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildLabelLogin(),
                  const SizedBox(
                    height: 20,
                  ),
                  _buildFormLogin(),
                  const SizedBox(
                    height: 20,
                  ),
                  _buildCheckboxShowPassword(),
                  const SizedBox(
                    height: 20,
                  ),
                  _buildLoginButton(),
                  const SizedBox(
                    height: 20,
                  ),
                  _buildLabelNotHaveAccount(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildLabelLogin() {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: const Text(
        'Welcome to the Login Page :)',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
      ),
    );
  }

  Widget _buildFormLogin() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            Container(
              width: double.infinity,
              color: Colors.transparent,
              child: PrimaryTextField(
                controller:
                    _authhenticationController.emailTextEditingController,
                hintText: 'Email',
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              width: double.infinity,
              color: Colors.transparent,
              child: PrimaryTextField(
                controller:
                    _authhenticationController.passwordTextEditingController,
                hintText: 'Password',
                obsecureText: _authhenticationController.isShowPassword?false:true,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
          width: double.infinity,
          height: 45,
          color: Colors.transparent,
          child: !_authhenticationController.isLoadingLogin
              ? PrimaryButton(
                  onPressed: () {
                    if (_authhenticationController
                            .emailTextEditingController.text.isNotEmpty &&
                        _authhenticationController
                            .passwordTextEditingController.text.isNotEmpty) {
                      _authhenticationController.signIn();
                    } else {
                      Fluttertoast.showToast(
                          msg: 'Form cannot be empty',
                          backgroundColor: Colors.red);
                    }
                  },
                  text: 'LOGIN',
                )
              : const PrimaryButtonLoading()),
    );
  }

  Widget _buildLabelNotHaveAccount() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
          width: double.infinity,
          height: 45,
          color: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const Text('Not have account ?'),
              const SizedBox(
                width: 5,
              ),
              GestureDetector(
                  onTap: () {
                    Get.toNamed(RegisterPage.routeName);
                  },
                  child: const Text(
                    'Register',
                    style: TextStyle(color: Colors.blue),
                  ))
            ],
          )),
    );
  }

  Widget _buildCheckboxShowPassword() {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: Row(
        children: [
          Checkbox(
              value: _authhenticationController.isShowPassword,
              onChanged: (v) {
                _authhenticationController.showPassword(v!);
              }),
          Expanded(
              child: Container(
                  color: Colors.transparent,
                  child: const Text('Show password')))
        ],
      ),
    );
  }
}
