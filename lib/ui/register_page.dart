import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:to_do_list_app/controller/authentication_controller.dart';
import 'package:to_do_list_app/reusable_widget/primary_button.dart';
import 'package:to_do_list_app/reusable_widget/primary_textfield.dart';

import '../reusable_widget/primary_button_loading.dart';

class RegisterPage extends StatelessWidget {
  RegisterPage({super.key});
  static const routeName = '/register-page';

  final AuthenticationController _authhenticationController =
      AuthenticationController.find;

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: _authhenticationController,
      builder: (controller) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            iconTheme: const IconThemeData(
              color: Colors.black, //change your color here
            ),
          ),
          body: SafeArea(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  _buildLabelRegister(),
                  const SizedBox(
                    height: 20,
                  ),
                  _buildFormRegister(),
                  const SizedBox(
                    height: 20,
                  ),
                  _buildCheckboxShowPassword(),
                  const SizedBox(
                    height: 20,
                  ),
                  _buildLoginRegister(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildLabelRegister() {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: const Text(
        'Please Register Here',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
      ),
    );
  }

  Widget _buildFormRegister() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            Container(
              width: double.infinity,
              color: Colors.transparent,
              child: PrimaryTextField(
                controller: _authhenticationController
                    .emailForRegisTextEditingController,
                hintText: 'Email',
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              width: double.infinity,
              color: Colors.transparent,
              child: PrimaryTextField(
                controller: _authhenticationController
                    .passwordForRegisTextEditingController,
                hintText: 'Password',
                obsecureText: _authhenticationController.isShowPasswordForRegis
                    ? false
                    : true,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              width: double.infinity,
              color: Colors.transparent,
              child: PrimaryTextField(
                controller: _authhenticationController
                    .passwordConfirmForRegisTextEditingController,
                hintText: 'Password Confirm',
                obsecureText:
                    _authhenticationController.isShowConfirmPasswordForRegis
                        ? false
                        : true,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginRegister() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
          width: double.infinity,
          height: 45,
          color: Colors.transparent,
          child: !_authhenticationController.isLoadingRegister
              ? PrimaryButton(
                  onPressed: () {
                    if (_authhenticationController
                            .emailForRegisTextEditingController
                            .text
                            .isNotEmpty &&
                        _authhenticationController
                            .passwordForRegisTextEditingController
                            .text
                            .isNotEmpty &&
                        _authhenticationController
                            .passwordConfirmForRegisTextEditingController
                            .text
                            .isNotEmpty) {
                      if (_authhenticationController
                              .passwordForRegisTextEditingController.text ==
                          _authhenticationController
                              .passwordConfirmForRegisTextEditingController
                              .text) {
                        _authhenticationController.signUp();
                      } else {
                        Fluttertoast.showToast(
                            msg: 'Password and Confirm password must be same',
                            backgroundColor: Colors.red);
                      }
                    } else {
                      Fluttertoast.showToast(
                          msg: 'Form cannot be empty',
                          backgroundColor: Colors.red);
                    }
                  },
                  text: 'REGISTER',
                )
              : const PrimaryButtonLoading()),
    );
  }

  Widget _buildCheckboxShowPassword() {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: Row(
        children: [
          Checkbox(
              value: _authhenticationController.isShowPasswordForRegis &&
                  _authhenticationController.isShowConfirmPasswordForRegis,
              onChanged: (v) {
                _authhenticationController.showPasswordAndConfirmPassword(v!);
              }),
          Expanded(
              child: Container(
                  color: Colors.transparent,
                  child: const Text('Show password')))
        ],
      ),
    );
  }
}
