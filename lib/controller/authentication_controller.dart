import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:to_do_list_app/ui/to_do_list_page.dart';

class AuthenticationController extends GetxController {
  static AuthenticationController find = Get.find<AuthenticationController>();
  bool isLoadingRegister = false;
  bool isSuccessRegister = false;

  bool isLoadingLogin = false;
  bool isSuccessLogin = false;

  final storage = GetStorage();

  TextEditingController emailTextEditingController =
      TextEditingController(text: '');
  TextEditingController passwordTextEditingController =
      TextEditingController(text: '');

  TextEditingController emailForRegisTextEditingController =
      TextEditingController(text: '');
  TextEditingController passwordForRegisTextEditingController =
      TextEditingController(text: '');

  TextEditingController passwordConfirmForRegisTextEditingController =
      TextEditingController(text: '');

  bool isShowPassword = false;
  bool isShowPasswordForRegis = false;
  bool isShowConfirmPasswordForRegis = false;

  void signUp() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    isLoadingRegister = true;
    update();
    try {
      UserCredential result = await auth.createUserWithEmailAndPassword(
          email: emailForRegisTextEditingController.text,
          password: passwordForRegisTextEditingController.text);
      if (result.additionalUserInfo!.isNewUser) {
        isLoadingRegister = false;
        isSuccessRegister = true;
        update();
        Fluttertoast.showToast(
            msg:
                'Hooray registration is successful, please try to login again :D',
            backgroundColor: Colors.blue);
        Get.back();
      } else {
        isLoadingRegister = false;
        isSuccessRegister = false;
        update();
      }
    } on FirebaseAuthException catch (e) {
      debugPrint('error signup: $e');
      Fluttertoast.showToast(msg: e.toString(), backgroundColor: Colors.red);
      isLoadingRegister = false;
      isSuccessRegister = false;
      update();
    }
  }

  void signIn() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    isLoadingLogin = true;
    update();
    try {
      UserCredential result = await auth.signInWithEmailAndPassword(
          email: emailTextEditingController.text,
          password: passwordTextEditingController.text);
      debugPrint('result signin: $result');
      if (!result.additionalUserInfo!.isNewUser) {
        storage.write('is_login', true);
        isLoadingLogin = false;
        isSuccessLogin = true;
        update();
        Get.offAndToNamed(ToDoListPage.routeName);
      } else {
        isLoadingLogin = false;
        isSuccessLogin = false;
        update();
      }
    } on FirebaseAuthException catch (e) {
      debugPrint('error signin: $e');
      Fluttertoast.showToast(msg: e.toString(), backgroundColor: Colors.red);
      isLoadingLogin = false;
      isSuccessLogin = false;
      update();
    }
  }

  void showPassword(bool status) {
    isShowPassword = status;
    update();
  }

  void showPasswordAndConfirmPassword(bool status) {
    isShowPasswordForRegis = status;
    isShowConfirmPasswordForRegis = status;
    update();
  }
}
