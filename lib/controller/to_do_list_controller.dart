import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:to_do_list_app/database/boxes.dart';
import 'package:to_do_list_app/database/hive_service.dart';
import 'package:to_do_list_app/model/task_model.dart';
import 'package:to_do_list_app/ui/login_page.dart';

class ToDoListController extends GetxController {
  static ToDoListController find = Get.find<ToDoListController>();
  List<TaskModel> listTask = <TaskModel>[];
  List<TaskModel> listTaskTemp = <TaskModel>[];

  List<String> listSort = ['Newest', 'Oldest', 'Task Name', 'Status'];
  RxString title = 'To Do List App'.obs;

  int id = 0;

  bool isEmpty = true;
  String initValue = 'Newest';
  SortBy selectSortBy = SortBy.NEWEST;
  TextEditingController taskTextEditingController = TextEditingController();
  TextEditingController keywordSearchTextEditingController =
      TextEditingController();

  TaskModel selectTask =
      TaskModel(id: 0, uuid: '', task: '', status: '', date: DateTime.now());

  final storage = GetStorage();

  int? indexList;

  late Box<TaskModel> taskBox;

  @override
  void onInit() {
    taskBox = Boxes.getTask();
    loadList();
    sortTask(SortBy.NEWEST);
    super.onInit();
  }

  void loadList() {
    if (taskBox.isNotEmpty) {
      debugPrint('box result: ${jsonEncode(taskBox.values.toList())}}');
      listTask = listOfTaskModelFromJson(jsonEncode(taskBox.values.toList()));
      listTaskTemp =
          listOfTaskModelFromJson(jsonEncode(taskBox.values.toList()));
      isEmpty = false;
      id = listTask
          .reduce((current, next) => current.id > next.id ? current : next)
          .id;
      update();
    }
  }

  void addTask(TaskModel task) {
    debugPrint('addTask');
    listTask.add(task);
    listTaskTemp.add(task);

    sortTask(selectSortBy);
    update();
    checkListIsEmpty();
    HiveService.addTaskToHive(task);
  }

  void editTask(String uuid, String task, int index) {
    debugPrint('editTask uuid: $uuid');
    debugPrint('editTask: $task');
    try {
      listTask.firstWhere((element) => element.uuid == uuid).task = task;
      listTaskTemp.firstWhere((element) => element.uuid == uuid).task = task;
      sortTask(selectSortBy);
      update();

      HiveService.updateTaskFromHive(index, listTask[index]);
    } catch (e) {
      debugPrint('editTask error: $e');
    }
  }

  void deleteTask(int id, int index) {
    debugPrint('delete task model: ${taskModelToJson(selectTask)}');
    try {
      listTask.removeWhere((element) => element.id == id);
      listTaskTemp.removeWhere((element) => element.id == id);
      debugPrint('index: $index');
      sortTask(selectSortBy);
      update();
      checkListIsEmpty();
      HiveService.deleteTaskFromHive(index);
    } catch (e) {
      debugPrint('deleteTask error: $e');
    }
  }

  void checkListIsEmpty() {
    if (listTask.isNotEmpty) {
      isEmpty = false;
    } else {
      isEmpty = true;
    }
  }

  void editTaskStatus(String uuid, TaskStatus taskStatus, int index) {
    try {
      listTask.firstWhere((element) => element.uuid == uuid).status =
          checkStatus(taskStatus);
      listTaskTemp.firstWhere((element) => element.uuid == uuid).status =
          checkStatus(taskStatus);

      update();
      HiveService.updateTaskFromHive(index, listTask[index]);
    } catch (e) {
      debugPrint('editTaskStatus error: $e');
    }
  }

  void searchTask(String keyword) {
    debugPrint('searchTask');
    try {
      List<TaskModel> result = listTaskTemp
          .where((element) =>
              element.task.toLowerCase().contains(keyword.toLowerCase()))
          .toList();
      listTask = result;
      debugPrint('result: $result');
      update();
      sortTask(selectSortBy);
    } catch (e) {
      debugPrint('searchTask error: $e');
    }
  }

  void sortTask(SortBy sortBy) {
    debugPrint('sortTask by : $sortBy');
    try {
      switch (sortBy) {
        case SortBy.NEWEST:
          listTask.sort((a, b) => a.date.compareTo(b.date));
          update();

          break;
        case SortBy.OLDEST:
          listTask.sort((b, a) => a.date.compareTo(b.date));
          update();

          break;
        case SortBy.TASK_NAME:
          listTask.sort((a, b) => a.task.compareTo(b.task));
          update();

          break;
        case SortBy.STATUS:
          listTask.sort((b, a) => a.status.compareTo(b.status));
          update();

          break;
        default:
          listTask.sort((a, b) => a.date.compareTo(b.date));
          update();
      }
    } catch (e) {
      debugPrint('sortTask error: $e');
    }
  }

  String checkStatus(TaskStatus taskStatus) {
    switch (taskStatus) {
      case TaskStatus.TODO:
        return 'to_do';
      case TaskStatus.PROGRESS:
        return 'progress';
      case TaskStatus.DONE:
        return 'done';
      default:
        return 'to_do';
    }
  }

  void selectedTask(TaskModel task) {
    selectTask = task;
    update();
  }

  int nextId() {
    return id++;
  }

  void updateInitValue(String value) {
    initValue = value;
    update();
  }

  void logoutApp() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    try {
      await auth.signOut();
      storage.write('is_login', false);
      Get.offAllNamed(LoginPage.routeName);
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString(), backgroundColor: Colors.red);
    }
  }

  void selectedIndex(int index) {
    indexList = index;
  }
}

enum TaskStatus { TODO, PROGRESS, DONE }

enum SortBy { NEWEST, OLDEST, TASK_NAME, STATUS }
