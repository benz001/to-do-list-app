import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:to_do_list_app/ui/login_page.dart';
import 'package:to_do_list_app/ui/to_do_list_page.dart';

class SplashPageController extends GetxController {
  static SplashPageController find = Get.find<SplashPageController>();

  String welcomeText = 'Welcome in To Do List App :)';

  final storage = GetStorage();

  @override
  void onReady() {
    checkAuthorization();
    super.onReady();
  }

  void checkAuthorization() {
    Future.delayed(const Duration(seconds: 1), () {
      bool? isLogin = storage.read('is_login');
      if (isLogin != null) {
        if (isLogin) {
          Get.offNamed(ToDoListPage.routeName);
        } else {
          Get.offNamed(LoginPage.routeName);
        }
      } else {
        Get.offNamed(LoginPage.routeName);
      }
    });
  }
}
