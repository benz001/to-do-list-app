import 'package:hive/hive.dart';
import 'package:to_do_list_app/model/task_model.dart';

class Boxes {
  static Box<TaskModel> getTask() => Hive.box<TaskModel>("task");
  
  // Futher,  you can add any other boxes here ...
}