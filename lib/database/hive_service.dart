import 'package:hive/hive.dart';
import 'package:to_do_list_app/database/boxes.dart';
import 'package:to_do_list_app/model/task_model.dart';

class HiveService {
  static Box taskBox = Boxes.getTask();

  static addTaskToHive(TaskModel task) async {
    await taskBox.add(task);
  }

  static updateTaskFromHive(int index, TaskModel task) async {
    await taskBox.putAt(index, task);
  }

  static deleteTaskFromHive(int index) async {
    await taskBox.deleteAt(index);
  }
}
