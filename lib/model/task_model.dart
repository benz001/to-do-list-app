// To parse this JSON data, do
//
//     final TaskModel = TaskModelFromJson(jsonString);

import 'dart:convert';
import 'package:hive/hive.dart';
part 'task_model.g.dart';

List<TaskModel> listOfTaskModelFromJson(String str) =>
    List<TaskModel>.from(
        json.decode(str).map((x) => TaskModel.fromJson(x)));

String listOfTaskModelToJson(List<TaskModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

TaskModel taskModelFromJson(String str) =>
    TaskModel.fromJson(json.decode(str));

String taskModelToJson(TaskModel data) => json.encode(data.toJson());

@HiveType(typeId: 0)
class TaskModel {
  TaskModel({
    required this.id,
    required this.uuid,
    required this.task,
    required this.status,
    required this.date,
  });

  @HiveField(0)
  int id;
  @HiveField(1)
  String uuid;
  @HiveField(2)
  String task;
  @HiveField(3)
  String status;
  @HiveField(4)
  DateTime date;

  factory TaskModel.fromJson(Map<String, dynamic> json) => TaskModel(
        id: json["id"],
        uuid: json["uuid"],
        task: json["task"],
        status: json["status"],
        date: DateTime.parse(json["date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "task": task,
        "status": status,
        "date": date.toIso8601String(),
      };
}
